﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField]
    private Texture2D[] images;
    private Texture2D image;

    [SerializeField]
    private GameObject whiteCube;
    [SerializeField]
    private GameObject redCube;
    [SerializeField]
    private GameObject blackCube;
    [SerializeField]
    private GameObject greenCube;
    [SerializeField]
    private GameObject yellowCube;
    [SerializeField]
    public int cubeValue;

    public class MyColors
    {
        static public Color yellow = new Color(1, 1, 0, 1);
    }


    private void Start()
    {
        image = images[Random.Range(0, images.Length)];

        Color[] pix = image.GetPixels();
        

        int worldX = image.width;
        int worldZ = image.height;


        Vector3[] spawnPositions = new Vector3[pix.Length];
        Vector3 startingSpawnPosition = new Vector3(-Mathf.Round(worldX / 2), 0, -Mathf.Round(worldZ / 2));
        Vector3 currentSpawnPos = startingSpawnPosition;

        int counter = 0;

        for (int z = 0; z < worldZ; z++)
        {
            for (int x = 0; x < worldX; x++)
            {
                spawnPositions[counter] = currentSpawnPos;
                counter++;
                currentSpawnPos.x++;
            }

            currentSpawnPos.x = startingSpawnPosition.x;
            currentSpawnPos.z++;
        }

        counter = 0;

        foreach (Vector3 pos in spawnPositions)
        {
            Color c = pix[counter];
            

            if (c.Equals(Color.clear))
            {
                
            }
            else if (c.a > 0)
            {
                if (c.Equals(Color.white))
                {
                    Instantiate(whiteCube, pos, Quaternion.identity);
                    cubeValue++;
                }

                if (c.Equals(Color.black))
                {
                    Instantiate(blackCube, pos, Quaternion.identity);
                    cubeValue++;
                }

                if (c.Equals(Color.red))
                {
                    Instantiate(redCube, pos, Quaternion.identity); 
                    cubeValue++;

                }

                if (c.Equals(MyColors.yellow))
                {
                    Instantiate(yellowCube, pos, Quaternion.identity);
                    cubeValue++;
                }

                if (c.Equals(Color.green))
                {
                    Instantiate(greenCube, pos, Quaternion.identity);
                    cubeValue++;
                }

            }

            counter++;
        }

    }



}







//[System.Serializable]
//public class ColorToPrefab
//{
//    public Color color;
//    public GameObject prefab;
//}

//[System.Serializable]
//public class LevelGenerator : MonoBehaviour
//{

//    public Texture2D map;
//    public ColorToPrefab[] colorMappings;


//    void Start()
//    {
//        GenerateLevel();
//    }

//    private void SpawnFromPool()
//    {

//        var cube = ObjectPooler.Instance.GetFromPool();
//        cube.transform.position = transform.position;

//    }

//    void GenerateLevel()
//    {
//        for (int z = 0; z < map.width; z++)
//        {
//            for (int x = 0; x < map.height; x++)
//            {
//                GenerateTile(x, z);
//            }
//        }
//    }

//    void GenerateTile(int x, int z)
//    {
//        Color pixelColor = map.GetPixel(x, z);

//        if (pixelColor.a == 0)
//        {
//            // the pixel is transparent, lets ignore it.
//            return;
//        }

//        //        Debug.Log(pixelColor);
//        foreach (ColorToPrefab colorMapping in colorMappings)
//        {
//            if (colorMapping.color.Equals(pixelColor))
//            {
//                Vector3 position = new Vector3(x, 0, z);
//                Instantiate(colorMapping.prefab, position, Quaternion.identity, transform);
//                SpawnFromPool();
//            }
//        }
//    }
//}
