﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ObjectPooler : MonoBehaviour
{
    
    public GameObject prefab;

    private Queue<GameObject> availableObjects = new Queue<GameObject>();

    public static ObjectPooler Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
        GrowPool();
    }

    public GameObject GetFromPool()
    {
        if (availableObjects.Count == 0)
            GrowPool();

        var instance = availableObjects.Dequeue();
        instance.SetActive(true);
        return instance;
    }

    private void GrowPool()
    {
        for (int i = 0; i < 256; i++)
        {
            var instanceToAdd = Instantiate(prefab);
            instanceToAdd.transform.SetParent(transform);
            AddToPool(instanceToAdd);
        }
    }


    public void AddToPool(GameObject instance)
    {
        instance.SetActive(false);
        availableObjects.Enqueue(instance);
    }

    
    //public List<Pool> pools;
    //public Dictionary<string, Queue<GameObject>> poolDictionary;
    //void Start()
    //{
    //    poolDictionary = new Dictionary<string, Queue<GameObject>>();

    //    foreach (Pool pool in pools)
    //    {
    //        Queue<GameObject> objectPool = new Queue<GameObject>();

    //        for (int i = 0; i < pool.size; i++)
    //        {
    //            GameObject obj = Instantiate(pool.prefab);
    //            obj.SetActive(false);
    //            objectPool.Enqueue(obj);
    //        }

    //        poolDictionary.Add(pool.tag, objectPool);
    //    }

    //}

    //public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
    //{
    //    if (!poolDictionary.ContainsKey(tag))
    //    {
    //        Debug.LogWarning("Pool with tag " + tag + " does not exist.");
    //        return null;
    //    }

    //    GameObject objectToSpawn = poolDictionary[tag].Dequeue();

    //    objectToSpawn.SetActive(true);
    //    objectToSpawn.transform.position = position;
    //    objectToSpawn.transform.rotation = rotation;

    //    poolDictionary[tag].Enqueue(objectToSpawn);

    //    return objectToSpawn;

    //}

}
