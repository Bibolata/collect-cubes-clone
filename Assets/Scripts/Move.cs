﻿using UnityEngine;

public class Move : MonoBehaviour
{

    public float speedModifier;
    public Rigidbody rB;
    public float speed;

    void Start()
    {
        

    }

    

    //Update is called once per frame
    void Update()
    {



        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved)
            {
                Vector3 direction = transform.forward + new Vector3(touch.deltaPosition.x, 0, touch.deltaPosition.y) * Time.deltaTime * speedModifier;
                rB.velocity = direction * speed;

                
                //    transform.position.x + touch.deltaPosition.x * speedModifier * Time.deltaTime,
                //    transform.position.y,
                //    transform.position.z + touch.deltaPosition.y * speedModifier * Time.deltaTime);

                

                //Vector3 direction = new Vector3(touch.deltaPosition.x + transform.position.x,
                //    transform.position.y, touch.deltaPosition.y + transform.position.z).normalized - transform.position;

                transform.rotation = Quaternion.LookRotation(direction);
                

            } 
            
            if(touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Stationary  || touch.phase == TouchPhase.Canceled)
            {
                rB.velocity = Vector3.zero;
                rB.angularVelocity = Vector3.zero;
            }
        }
    }
}

