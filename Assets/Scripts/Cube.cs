﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    public GameObject collector;
    public float force;
    public float friction = 0.4f;
    public int currentProgression;
    
    public ProgressBar progressBar;
    


    // Start is called before the first frame update
    void Start()
    {
        collector = GameObject.Find("Collector");
        int numberOfTaggedObjects = GameObject.FindGameObjectsWithTag("Cube").Length;
        Debug.Log("Here are " + numberOfTaggedObjects + "cubes.");

    }


    // Update is called once per frame
    void Update()
    {
        

        gameObject.GetComponent<Rigidbody>().velocity *= friction;
        
        if (gameObject.layer == 13)
        {
            gameObject.GetComponent<Rigidbody>().AddForce((collector.transform.position - gameObject.transform.position) * force, ForceMode.Impulse);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 12)
        {
            gameObject.layer = 13;
            gameObject.tag = "Untagged";
            gameObject.GetComponent<Renderer>().material.color = Color.cyan;
            
        }
    }

    
}
