﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    
    public Slider slider;
    private LevelGenerator levelGeneratorScript;
    

    

    

    public void SetMaxProgression(int progression)
    {
        slider.maxValue = progression;
        slider.value = 0;
    }

    public void SetProgression(int progression)
    {
        slider.value = progression;
    }


    //public Text text;

    //public float fillSpeed = 0.5f;
    //public float targetProgress = 0;
    

    //private void Awake()
    //{
    //    slider = gameObject.GetComponent<Slider>();
    //}

    //private void Update()
    //{
    //    if (slider.value < targetProgress)
    //        slider.value += fillSpeed * Time.deltaTime;
    //}

    //// Add the progress bar.
    //public void IncrementProgress(float newProgress)
    //{
    //    targetProgress = slider.value + newProgress;
    //}
}
